/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  if (sporocilo.charAt(0) != ('(')) {
    var name = sporocilo.substr(0, Math.min(sporocilo.indexOf(':'), sporocilo.indexOf('(zasebno)') - 1));
    var zamenjava = '';
    var zamenjaj = false;
    for (var i = 0; i < shranjeniUporabniki.length; i++) {
      if (shranjeniUporabniki[i] == name) {
        zamenjava = shranjeniNadimki[i];
        zamenjaj = true;
        break;
      }
    }
    if (zamenjaj) {
      sporocilo = sporocilo.replace(name, zamenjava + ' (' + name + ')');
    }
  }
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    var split = sporocilo.split(' <br/ >');
    sporocilo = split[0];
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo + (split.length == 2 ? ' <br/ >' + split[1]: ''));
  } else {
    return (sporocilo.indexOf('&#9756') >= 0 || sporocilo.indexOf('<br/ >') >= 0) ? divElementHtmlTekstBold(sporocilo): $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekstBold(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    var slikeURL = sporocilo.match(/(\bhttp\S+\.(?:jpg|png|gif)\b)/g);
    if (slikeURL) {
      sporocilo += ' ';
      for (var i = 0; i < slikeURL.length; i++) {
        if (slikeURL[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") <= -1) {
          sporocilo += '<br/ ><img src=\"' +  slikeURL[i] + '\" style=\"width: 200px; padding-left: 20px;\"></img>';
        }
      }
    }
    
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append((slikeURL) ? divElementHtmlTekstBold(sporocilo): divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Poslušaj za spremembe vzdevkov
  socket.on('spremembaVzdevka', function (sporocilo) {
    for (var i = 0; i < shranjeniUporabniki.length; i++) {
      console.log(shranjeniUporabniki[i] + ' | ' + sporocilo.starVzd);
      if (shranjeniUporabniki[i] == sporocilo.starVzd) {
        shranjeniUporabniki[i] = sporocilo.novVzd;
      }
    }
  });
  
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var zeObstaja = false;
      var j;
      for (j = 0; j < shranjeniUporabniki.length; j++) {
        if (shranjeniUporabniki[j] == uporabniki[i]) {
          zeObstaja = true;
          break;
        }
      }
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(zeObstaja ? shranjeniNadimki[j] + ' (' + uporabniki[i] + ')': uporabniki[i]));
    }
    
    $('#seznam-uporabnikov div').click(function() {
      var ime = $(this).text();
      var procSpor = klepetApp.procesirajUkaz('/zasebno \"' + (ime.indexOf(' ') >= 0 ? ime.substring(ime.indexOf(' ') + 2, ime.length - 1): ime) + '\" \"&#9756;\"');
      $('#sporocila').append(divElementHtmlTekst(procSpor));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
