// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika

var shranjeniNadimki = [];
var shranjeniUporabniki = [];

var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var slikeURL = besedilo.match(/(\bhttp\S+\.(?:jpg|png|gif)\b)/g);
      var parametri = besedilo.split('\"');
      
      if (parametri) {
        var znanUpor = false;
        var j;
        for (j = 0; j < shranjeniUporabniki.length; j++) {
          if (shranjeniUporabniki[j] == parametri[1]) {
            znanUpor = true;
            break;
          }
        }
        if (slikeURL) {
          var dodSporocilo = ' ';
          for (var i = 0; i < slikeURL.length; i++) {
            if (slikeURL[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") <= -1) {
              dodSporocilo += '<br/ ><img src=\"' +  slikeURL[i] + '\" style=\"width: 200px; padding-left: 20px;\"></img>';
            }
          }
        }
        
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3] + (slikeURL ? dodSporocilo: '')});
        sporocilo = '(zasebno za ' + (znanUpor ? shranjeniNadimki[j] + ' (' + parametri[1] + ')': parametri[1]) + '): ' + parametri[3] + (slikeURL ? dodSporocilo: '');
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      besede.shift();
      if (besede.length == 1) {
        sporocilo = 'Barva sprememenjena.'
        document.querySelector('#sporocila').style.backgroundColor = besede[0];
        document.querySelector('#kanal').style.backgroundColor = besede[0];
        if (!isColorValid(document.querySelector('#sporocila'), besede[0])) {
          sporocilo = 'Barva ne obstaja.'
        } 
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj':
      besede.shift();
      var narejeno = false;
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      
      if (parametri) {
        for (var i = 0; i < shranjeniUporabniki.length; i++) {
          if (shranjeniUporabniki[i] == parametri[1]) {
            shranjeniNadimki[i] == parametri[3];
            narejeno = true;
          }
        }
      
        if (!narejeno) {
          shranjeniUporabniki[shranjeniUporabniki.length] = parametri[1];
          shranjeniNadimki[shranjeniNadimki.length] = parametri[3];
        }
        sporocilo = 'Uporabnik ' + parametri[1] + ' preimenovan v ' + parametri[3] + '.';
        } else {
          sporocilo = 'Neznan ukaz';
        }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

function isColorValid(element, value) {
    return element && element.style.backgroundColor === value;
}